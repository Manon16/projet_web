<?php
session_start();
include('accountFunctions.php');
testAndSetCookies();
if (!isset($_SESSION['log'])) {
	$_SESSION['log'] = FALSE;
	header('Location: ../studies/homePage.php');
}
if ($_SESSION['cat'] != 'administrator') {
	header('Location: ../studies/homePage.php');
}
?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<link rel="stylesheet" href="../fixstyle.css" />
		<title> ClinicalTrialsByMLG - account creation </title>
	</head>

	<body>
		<?php
			include('../header.php');
			/*echo '<pre>';
			print("</br> SESSION['error'] "); print_r($_SESSION['error']);
			print("</br> POST "); print_r($_POST);
			echo '</pre>';*/
		?>
		<div class='inner-body edit-and-create-account-page' id='create-account-page'>


		<section id='create-account-section'>
			<fieldset> <legend> Account creation </legend>

				<form method="POST" action="accountCreationFormByAdmin.php" class="form-stlye-1">	
					<label for="cat"> <span> User category <span class='required'>*</span> </span>
					<select id="cat" name="creation[cat]" class='admin-cat-choice'>
						<option value="null"> -- </option>
						<option value="moderator" 
							<?php if (isset($_POST['creation']['cat']) AND $_POST['creation']['cat']=="moderator") {print("selected");}?> 
						> Moderator </option>
						<option value="sponsor"
							<?php if (isset($_POST['creation']['cat']) AND $_POST['creation']['cat']=="sponsor") {print("selected");}?> 
						> Sponsor </option>
						<option value="authority"
							<?php if (isset($_POST['creation']['cat']) AND $_POST['creation']['cat']=="authority") {print("selected");}?>
						> Authority </option>
						<option value="visitor"
							<?php if (isset($_POST['creation']['cat']) AND $_POST['creation']['cat']=="visitor") {print("selected");}?>
						> Visitor </option>
					</select> 
					<input type="submit" name="creation[btn]" value="OK" class='select admin-cat-choice'/>
					</label>
				</form>

				<form method="POST" action="accountCreationFormByAdmin.php" class="form-stlye-1">
					<?php 
						if (isset($_POST['creation']['cat']) AND $_POST['creation']['cat'] !="null") {
							printf("<input type='hidden' name='creation[cat]' value='%s'/>",$_POST['creation']['cat']);
							if ($_POST['creation']['cat'] == "authority" OR $_POST['creation']['cat'] == "sponsor") {
								if ($_POST['creation']['btn'] == 'Sign In') {$institutVal=$_POST['creation']['institut'];} else {$institutVal="";}
								printf("
									<label for='institut'> <span> Institut <span class='required'>*</span> </span>
									<input id='institut' type='text' name='creation[institut]' value='%s' required/>
									</label>", $institutVal);
							} 
							if ($_POST['creation']['cat'] != "authority") {
								if ($_POST['creation']['btn'] == 'Sign In') {$fnameVal=$_POST['creation']['first_name'];} else {$fnameVal="";}
								if ($_POST['creation']['btn'] == 'Sign In') {$lnameVal=$_POST['creation']['last_name'];} else {$lnameVal="";}
								printf("
								<label for='first_name'> <span> First Name <span class='required'>*</span> </span>
								<input id='first_name' type='text' name='creation[first_name]' value='%s' required/>
								</label>
								<label for='last_name'> <span> Last Name <span class='required'>*</span> </span>
								<input id='last_name' type='text' name='creation[last_name]' value='%s' required/> 
								</label>", $fnameVal,$lnameVal);
							}
							if ($_POST['creation']['btn'] == 'Sign In') {$mailVal=$_POST['creation']['mail'];} else {$mailVal="";}
							printf("
							<label for='mail'> <span> e-mail <span class='required'>*</span> </span>
							<input id='mail' type='email' name='creation[mail]' value='%s' required/> 
							</label>", $mailVal);
							if (isset($_SESSION['error']['email-exists'])) {
								if ($_SESSION['error']['email-exists']) {
									print("<p> This email is already taken </p>");
								}
							}
							if ($_POST['creation']['btn'] == 'Sign In') {$countryVal=$_POST['creation']['country'];} else {$countryVal="";}
							if ($_POST['creation']['btn'] == 'Sign In') {$pwdVal=$_POST['creation']['pwd'];} else {$pwdVal="";}
							printf("
							<label for='country'> <span> Country <span class='required'>*</span> </span>
							<input id='country' type='text' name='creation[country]' value='%s' required/>
							</label>
							<label for='pwd'> <span> Password <span class='required'>*</span> <span class='additional-info'>(6 chars min)</span> </span>
							<input id='pwd' type='password' name='creation[pwd]' minlength='6' value='%s' required/>
							</label>
							<label for='pwdconf'> <span> Password confirmation <span class='required'>*</span> </span>
							<input id='pwdconf' type='password' name='creation[pwdconf]' minlength='6' value='' required/>
							</label>", $countryVal,$pwdVal);
							if (isset($_SESSION['error']['bad-pwd-conf'])) {
								if ($_SESSION['error']['bad-pwd-conf']) {
									print('<p> Password not coherent </p>');
								} 
							}
							print("
							<div class='form-confirmation'>
							<button type='submit' name='creation[btn]' value='create-admin' class='submit'> Sign In </button>");
						}
					?>
				</form>
				<form method="POST" action="accountCreationFormByAdmin.php">
					<?php
						if (isset($_POST['creation']['cat']) AND $_POST['creation']['cat'] !="null") {
							print("<input type='submit' name='creation[btn]' value='Reset' class='reset'/>");
							print("</div>");
						}
					?>
				</form>

			</fieldset>
		</section>

		</div>
		<?php
			include('../footer.php');
		?>	
	</body>
</html>
