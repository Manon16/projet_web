<?php
session_start();
include('accountFunctions.php');
testAndSetCookies();
if (!isset($_SESSION['log'])) {
	$_SESSION['log'] = FALSE;
	header('Location: ../studies/homePage.php');
}
if ($_SESSION['cat'] != 'administrator') {
	header('Location: ../studies/homePage.php');
}
?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<link rel="stylesheet" href="../fixstyle.css" />
		<title> ClinicalTrialsByMLG - profile edition </title>
	</head>

	<body>
		<?php
			include('../header.php');
		?>
		<div class='inner-body edit-and-create-account-page' id='edit-profile-page'>

		<section id='edit-profile-admin-section'>
			<fieldset> <legend> Account edition </legend>

				<form method="POST" action="accountEditionFormByAdmin.php" class="form-stlye-1">	
					<label for="cat"> <span> User category <span class='required'>*</span> </span>
					<select id="cat" name="edition[cat]" class='admin-cat-choice'>
						<?php 
							if (!isset($_POST['edition']['cat'])) {
								$_POST['edition']['cat'] = $_SESSION['management']['cat'];
								$_POST['edition']['btn'] = "";
							}
						?>
						<option value="moderator" 
							<?php if (isset($_POST['edition']['cat']) AND $_POST['edition']['cat']=="moderator") {print("selected");}?> 
						> Moderator </option>
						<option value="sponsor"
							<?php if ((isset($_POST['edition']['cat']) AND $_POST['edition']['cat']=="sponsor")) {print("selected");}?> 
						> Sponsor </option>
						<option value="authority"
							<?php if (isset($_POST['edition']['cat']) AND $_POST['edition']['cat']=="authority") {print("selected");}?>
						> Authority </option>
						<option value="visitor"
							<?php if (isset($_POST['edition']['cat']) AND $_POST['edition']['cat']=="visitor") {print("selected");}?>
						> Visitor </option>
					</select> 
					<input type="submit" name="edition[btn]" value="OK" class='select admin-cat-choice'/>
					</label>
				</form>

				<form method="POST" action="accountEditionFormByAdmin.php" class="form-stlye-1">
					<?php 
						if (isset($_POST['edition']['cat']) AND $_POST['edition']['cat'] !="null") {
							printf("<input type='hidden' name='edition[cat]' value='%s'/>",$_POST['edition']['cat']);
							if ($_POST['edition']['cat'] == "authority" OR $_POST['edition']['cat'] == "sponsor") {
								if ($_POST['edition']['btn'] == 'edit-admin') {$institutVal=$_POST['edition']['institut'];} else {$institutVal=$_SESSION['management']['institut'];}
								printf("
									<label for='institut'> <span> Institut <span class='required'>*</span> </span>
									<input id='institut' type='text' name='edition[institut]' value='%s' required/>
									</label>", $institutVal);
							} 
							if ($_POST['edition']['cat'] != "authority") {
								if ($_POST['edition']['btn'] == 'edit-admin') {$fnameVal=$_POST['edition']['first_name'];} else {$fnameVal=$_SESSION['management']['first_name'];}
								if ($_POST['edition']['btn'] == 'edit-admin') {$lnameVal=$_POST['edition']['last_name'];} else {$lnameVal=$_SESSION['management']['last_name'];}
								printf("
								<label for='first_name'> <span> First Name <span class='required'>*</span> </span>
								<input id='first_name' type='text' name='edition[first_name]' value='%s' required/>
								</label>
								<label for='last_name'> <span> Last Name <span class='required'>*</span> </span>
								<input id='last_name' type='text' name='edition[last_name]' value='%s' required/> 
								</label>", $fnameVal,$lnameVal);
							}
							if ($_POST['edition']['btn'] == 'edit-admin') {$mailVal=$_POST['edition']['mail'];} else {$mailVal=$_SESSION['management']['mail'];}
							printf("
							<label for='mail'><span>  e-mail <span class='required'>*</span> </span>
							<input id='mail' type='email' name='edition[mail]' value='%s' required/> 
							</label>", $mailVal);
							if (isset($_SESSION['error']['email-exists'])) {
								if ($_SESSION['error']['email-exists']) {
									print("<p class='text-error'> This email is already taken </p>");
								}
							}
							if ($_POST['edition']['btn'] == 'edit-admin') {$countryVal=$_POST['edition']['country'];} else {$countryVal=$_SESSION['management']['country'];}
							printf("
							<label for='country'> <span> Country <span class='required'>*</span> </span>
							<input id='country' type='text' name='edition[country]' value='%s' required/>
							</label>", $countryVal);
							$lastPwdVal=$_SESSION['management']['pwd'];
							if ($_POST['edition']['btn'] == 'edit-admin') {$pwdVal=$_POST['edition']['new_pwd'];} else {$pwdVal=$_SESSION['management']['pwd'];}
							if ($_POST['edition']['btn'] == 'edit-admin') {
								if (!$_SESSION['error']['bad-pwd-conf']) {
									if ($pwdVal==$lastPwdVal) {$confPwdVal=$lastPwdVal;} else {$confPwdVal=$pwdVal;}} else {$confPwdVal='';}
							} else {$confPwdVal=$lastPwdVal;}
								
							printf("
							<label for='pwd'> <span> Last password <span class='required'>*</span> </span>
							<input id='pwd' type='text' name='edition[last_pwd]' minlength='6' value='%s' readonly/>
							</label>
							<label for='pwd'> <span> Password <span class='required'>*</span> <span class='additional-info'>(6 chars min)</span> </span> 
							<input id='pwd' type='password' name='edition[new_pwd]' minlength='6' value='%s' required/>
							</label>
							<label for='pwdconf'> <span> Password confirmation <span class='required'>*</span> </span> 
							<input id='pwdconf' type='password' name='edition[pwdconf]' minlength='6' value='%s' required/>
							</label>", $lastPwdVal,$pwdVal,$confPwdVal);
							if (isset($_SESSION['error']['bad-pwd-conf'])) {
								if ($_SESSION['error']['bad-pwd-conf']) {
									print("<p class='text-error'> Password not coherent </p>");
								} 
							}
							print("
							<div class='form-confirmation'>
								<button type='submit' name='edition[btn]' value='edit-admin' class='submit'> Edit </button>
								<input type='reset' name='edition[btn]' value='Reset' class='reset'/>
							</div>");
						}
					?>
				</form>

			</fieldset>
		</section>

		</div>
		<?php
			include('../footer.php');
		?>	
	</body>
</html>