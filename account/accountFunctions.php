<?php
include('../functions.php');
?>


<?php // vérifier tous les $_SESSION['error'] !!!
	// on teste si le visiteur a soumis les différents formulaires de connexion/creation/déconnexion...
	if (isset($_POST['creation']) AND $_POST['creation']['btn'] == 'create-visitor') { //page: logInSignInForm.php
		unset($_SESSION['error']['bad-email'],$_SESSION['error']['bad-pwd'],$_SESSION['error']['connection']);
		createAccount("logInSignInForm.php");
	}
	if (isset($_POST['creation']) AND $_POST['creation']['btn'] == 'create-admin') { //page: accountCreationFormByAdmin.php
		unset($_SESSION['error']['bad-email'],$_SESSION['error']['bad-pwd'],$_SESSION['error']['connection']);
		createAccount("accountManagement.php");
	}
	if (isset($_POST['creation']) AND $_POST['creation']['btn'] == 'Reset') {
		unset($_SESSION['error']['email-exists'],$_SESSION['error']['bad-pwd-conf'],$_SESSION['error']['creation']);
	}
	if (isset($_POST['creation']) AND $_POST['creation']['btn'] == 'OK') {
		unset($_SESSION['error']['email-exists'],$_SESSION['error']['bad-pwd-conf'],$_SESSION['error']['creation']);
	}
	if (isset($_POST['connection']['btn']) AND $_POST['connection']['btn'] == 'Log In') { //page: logInSignInForm.php
		unset($_SESSION['error']['email-exists'],$_SESSION['error']['bad-pwd-conf'],$_SESSION['error']['creation']);
		login();
	}
	if (isset($_POST['connection']) AND $_POST['connection']['btn'] == 'Reset') {
		unset($_SESSION['error']['bad-email'],$_SESSION['error']['bad-pwd'],$_SESSION['error']['connection']);
	}
	if (isset($_POST['connection']) AND $_POST['connection']['btn'] == 'Log Out') { //header
		logout('../studies/homePage.php');
	}
	if (isset($_POST['edition']) AND $_POST['edition']['btn'] == 'Edit') { //page: accountOwnEditionForm.php
		editAccount($_SESSION['mail'],'profilePage.php');
	}
	if (isset($_POST['edition']) AND $_POST['edition']['btn'] == 'Change') { //page: accountOwnEditionForm.php
		editPassword($_SESSION['mail'],'profilePage.php');
	}
	if (isset($_POST['deletion']) AND $_POST['deletion']['btn'] == 'conf-user') {//pages: profilePage.php 
		// confirmation de suppression de compte
		deleteAccount($_SESSION['mail'],'logInSignInForm.php');
	}
	if (isset($_POST['deletion']) AND $_POST['deletion']['btn'] == 'conf-admin') {//pages: accountManagement.php
		// confirmation de suppression de compte
		foreach($_POST['selection']['mail'] as $mail) {
			deleteAccount($mail,'accountManagement.php');
		}
	}
	if (isset($_POST['edition']) AND $_POST['edition']['btn'] == 'edit-admin') { //page: accountEditionFormByAdmin.php
		unset($_SESSION['error']['email-exists'], $_SESSION['error']['profile-change'], $_SESSION['error']['bad-pwd'], $_SESSION['error']['pwd-change']);
		editAccount($_SESSION['management']['mail'],'');
		editPassword($_SESSION['management']['mail'],'');
		if(!$_SESSION['error']['profile-change'] AND !$_SESSION['error']['pwd-change']) {
			header('Location: accountManagement.php');
			unset($_SESSION['error']); $_SESSION['error'] = array();
		}
	}
//	if (isset($_POST['deletion']) AND $_POST['deletion']['btn'] == 'Delete') {
//		// demande de supression de compte
//	}
//	if (isset($_POST['edition']) AND $_POST['edition']['btn'] == 'Edit a profile') { //page: accountManagement.php
//		// demande d'édition de compte par les admins
//	}
?>


<?php

	function createAccount($link) {
		// Récupération des variables d'intérêt
		if (isset($_POST['creation']['cat'])) {$usercat = sprintf("'%s'",$_POST['creation']['cat']);} 
		else {$usercat = "'visitor'";}
		if (isset($_POST['creation']['institut'])) {$institut = sprintf("'%s'",$_POST['creation']['institut']);}
		else {$institut = 'NULL';}
		if (isset($_POST['creation']['first_name'])) {$first_name = sprintf("'%s'",$_POST['creation']['first_name']);}
		else {$first_name = 'NULL';}
		if (isset($_POST['creation']['last_name'])) {$last_name = sprintf("'%s'",$_POST['creation']['last_name']);}
		else {$last_name = 'NULL';}
		$mail = sprintf("'%s'",$_POST['creation']['mail']);
		$country = sprintf("'%s'",$_POST['creation']['country']);
		$pwd = sprintf("'%s'",$_POST['creation']['pwd']);
		$conf = sprintf("'%s'",$_POST['creation']['pwdconf']);
		// Vérification de l'email entré (il ne doit pas déjà être dans la bdd)
		$queryEmailCheck = sprintf("SELECT Email FROM `users` WHERE Email = %s", $mail);
		$resEmailCheck = requestS($queryEmailCheck);
		if (!array_key_exists('error', $resEmailCheck)) {
			if (empty($resEmailCheck)) {
				$_SESSION['error']['email-exists'] = FALSE;
			} else {
				$_SESSION['error']['email-exists'] = TRUE;
			}
		} else {
			print("<p> Problem de query ou de connexion à la bdd </p>");
		}
		// Vérification de la concordance entre le mdp et sa confirmation
		if ($pwd == $conf) {
			$_SESSION['error']['bad-pwd-conf'] = FALSE;
		} else {
			$_SESSION['error']['bad-pwd-conf'] = TRUE;
		}
		// Création du compte
		if (!$_SESSION['error']['bad-pwd-conf'] AND !$_SESSION['error']['email-exists']) {
			$queryCreation = sprintf("INSERT INTO `users` (`Email`, `Pwd`, `User_Cat`, `First_Name`, `Last_Name`, `Country`, `Institut`) VALUES (%s,%s,%s,%s,%s,%s,%s)",$mail,$pwd,$usercat,$first_name,$last_name,$country,$institut);
			$resCreation = requestTF($queryCreation);
			if (empty($resCreation)) {
				$_SESSION['error']['creation'] = FALSE;
				// Redirection si succès
				if (!empty($link)) {header("Location: $link");}
			} else {
				$_SESSION['error']['creation'] = TRUE;
			}
		} else {
			$_SESSION['error']['creation'] = TRUE;
		}
	}


	function login() {
		// Récupération des variables d'intérêt
		$mail = $_POST['connection']['mail'];
		$pwd = $_POST['connection']['pwd'];
		// Vérification de l'email
		$queryEmailCheck = sprintf("SELECT * FROM `users` WHERE Email = '%s'", $mail);
		$resIdentification = requestS($queryEmailCheck);
		if (!array_key_exists('error', $resIdentification)) {
			if (!empty($resIdentification)) {
				$_SESSION['error']['bad-email'] = FALSE;
				// Vérification du mdp
				if ($resIdentification[0]['Pwd'] == $pwd) {
					$_SESSION['error']['bad-pwd'] = FALSE;
					$_SESSION['error']['connection'] = FALSE;
					// En cas de succès: set les variables de la session et renvoie à la page d'accueil
					$_SESSION['log'] = TRUE;
					$_SESSION['idUser'] = $resIdentification[0]['idUser'];
					$_SESSION['cat'] = $resIdentification[0]['User_Cat'];
					if ($resIdentification[0]['First_Name'] != NULL) {$_SESSION['first_name'] = $resIdentification[0]['First_Name'];}
					if ($resIdentification[0]['Last_Name'] != NULL) {$_SESSION['last_name'] = $resIdentification[0]['Last_Name'];}
					if ($resIdentification[0]['Institut'] != NULL) {$_SESSION['institut'] = $resIdentification[0]['Institut'];}
					$_SESSION['mail'] = $resIdentification[0]['Email'];
					$_SESSION['country'] = $resIdentification[0]['Country'];
					// set les cookies si demandé
					if (isset($_POST['connection']['cookie'])) {
						$endtime = time() + 7*24*3600;
						setcookie('expiration', $endtime, $endtime, '/', null, false, true);
						setcookie('idUser', $resIdentification[0]['idUser'], $endtime, '/', null, false, true);
						setcookie('cat', $resIdentification[0]['User_Cat'], $endtime, '/', null, false, true);
						if ($resIdentification[0]['First_Name'] != NULL) {setcookie('first_name', $resIdentification[0]['First_Name'], $endtime, '/', null, false, true);}
						if ($resIdentification[0]['Last_Name'] != NULL) {setcookie('last_name', $resIdentification[0]['Last_Name'], $endtime, '/', null, false, true);}
						if ($resIdentification[0]['Institut'] != NULL) {setcookie('institut', $resIdentification[0]['Institut'], $endtime, '/', null, false, true);}
						setcookie('mail', $resIdentification[0]['Email'], $endtime,'/', null, false, true);
						setcookie('country', $resIdentification[0]['Country'], $endtime, '/', null, false, true);
					}
					header('Location: ../studies/homePage.php'); 
				} else {
					$_SESSION['error']['bad-pwd'] = TRUE;
					$_SESSION['error']['connection'] = TRUE;
				}
			} else {
				$_SESSION['error']['bad-email'] = TRUE;
				$_SESSION['error']['connection'] = TRUE;
			}
		} else {
			print("Problem de query ou de connexion à la bdd");
		}
	}

	function logout($link) {
		//écrasement des cookies par des cookies vides
		setcookie('expiration',"",1,'/'); unset($_COOKIE['expiration']);
		setcookie('idUser',"",1,'/'); unset($_COOKIE['idUser']);
		setcookie('cat',"",1,'/'); unset($_COOKIE['cat']);
		setcookie('first_name',"",1,'/'); unset($_COOKIE['first_name']);
		setcookie('last_name',"",1,'/'); unset($_COOKIE['last_name']);
		setcookie('institut',"",1,'/'); unset($_COOKIE['institut']);
		setcookie('mail',"",1,'/'); unset($_COOKIE['mail']);
		setcookie('country',"",1,'/'); unset($_COOKIE['country']);
		//initialisation d'une session non connectée
		session_destroy();
		session_start();
		$_SESSION['log'] = FALSE;
		$_SESSION['cat'] = 'ext';
		//redirection
		header("Location: $link");
	}


	function deleteAccount($mail,$link) {
		$queryDeletion = sprintf("DELETE FROM `users` WHERE Email = '%s'", $mail);
		$resDeletion = requestTF($queryDeletion);
		if (empty($resDeletion)) {
			if ($mail == $_SESSION['mail']) { // si un utilisateur supprime son propre compte
				logout('logInSignInForm.php');
			} 
			$_SESSION['error']['deletion'] = FALSE;
			header("Location: $link");
		} else {
			$_SESSION['error']['deletion'] = TRUE;
			print("Problème lors de la suppression ou de la connexion à la bdd");
		}
	}


	function editAccount($mailref,$link) {
		// Récupération des variables d'intérêt
		if (isset($_POST['edition']['cat'])) {$usercat = sprintf("'%s'",$_POST['edition']['cat']);} 
		else {$usercat = sprintf("'%s'",$_SESSION['cat']);}
		if (isset($_POST['edition']['institut'])) {$institut = sprintf("'%s'",$_POST['edition']['institut']);}
		else {$institut = 'NULL';}
		if (isset($_POST['edition']['first_name'])) {$first_name = sprintf("'%s'",$_POST['edition']['first_name']);}
		else {$first_name = 'NULL';}
		if (isset($_POST['edition']['last_name'])) {$last_name = sprintf("'%s'",$_POST['edition']['last_name']);}
		else {$last_name = 'NULL';}
		$mail = sprintf("'%s'",$_POST['edition']['mail']);
		$country = sprintf("'%s'",$_POST['edition']['country']);
		// Editions des colonnes de la table users (nom, prénom et pays uniquement si au moins 1 des 3 est modifié)
//		if ($first_name != $_SESSION['first_name'] OR $last_name != $_SESSION['last_name'] OR $country != $_SESSION['country']) {
//			$queryInfoChange = sprintf("UPDATE `users` SET (First_Name, Last_Name, Country) = ('%s','%s','%s') WHERE Email = '%s'", $first_name,$last_name,$country,$_SESSION['mail']);
//		}
		// Edition de l'email dans toutes les tables de la bdd l'utilisant (uniquement si différent de l'actuel email)
//		if ($mail != $_SESSION['mail']) {
			// Vérification de l'email entré (il ne doit pas déjà être dans la bdd)
			$queryEmailCheck = sprintf("SELECT Email FROM `users` WHERE Email = %s", $mail);
			$resEmailCheck = requestS($queryEmailCheck);
			if (!array_key_exists('error', $resEmailCheck)) {
				if (empty($resEmailCheck) OR $resEmailCheck[0]['Email'] == $mailref) {
					$_SESSION['error']['email-exists'] = FALSE;
					// Edition de l'email
					$queryChange = sprintf("UPDATE `users` SET Email=%s, User_Cat=%s, First_Name=%s, Last_Name=%s, Country=%s, Institut=%s WHERE Email='%s'", $mail,$usercat,$first_name,$last_name,$country,$institut,$mailref);
					$resChange = requestTF($queryChange);
					if (empty($resChange)) {
						if ($mailref == $_SESSION['mail']) { // modif de son propre compte
							// test si cookies de session existent
							if (isset($_COOKIE['mail'])) {
								$cookie = TRUE;
								$endtime = $_COOKIE['expiration'];
							}
							// update les variables session et cookie
							if ($first_name != 'NULL') {
								$_SESSION['first_name'] = $_POST['edition']['first_name']; 
								if ($cookie) {setcookie('first_name', $_SESSION['first_name'], $endtime, '/', null, false, true);}
							} else {unset($_SESSION['first_name']);}
							if ($last_name != 'NULL') {
								$_SESSION['last_name'] = $_POST['edition']['last_name'];
								if ($cookie) {setcookie('last_name', $_SESSION['last_name'], $endtime, '/', null, false, true);}
							} else {unset($_SESSION['last_name']);}
							if ($institut != 'NULL') {
								$_SESSION['institut'] = $_POST['edition']['institut'];
								if ($cookie) {setcookie('institut', $_SESSION['institut'], $endtime, '/', null, false, true);}
							} else {unset($_SESSION['institut']);}
							$_SESSION['mail'] = $_POST['edition']['mail'];
							if ($cookie) {setcookie('mail', $_SESSION['mail'], $endtime, '/', null, false, true);}
							$_SESSION['country'] = $_POST['edition']['country'];
							if ($cookie) {setcookie('country', $_SESSION['country'], $endtime, '/', null, false, true);}
						}
						$_SESSION['error']['profile-change'] = FALSE;
						if (!empty($link)) {header("Location: $link");}
					} else {
						$_SESSION['error']['profile-change'] = TRUE;
						print("<p> Problem de query ou de connexion à la bdd </p>");
					}
				} else {
					$_SESSION['error']['email-exists'] = TRUE;
					$_SESSION['error']['profile-change'] = TRUE;
				}
			} else {
				print("<p> Problem de query ou de connexion à la bdd </p>");
			}	
//		}
	}

	function editPassword($mail,$link) {
		$lastpwd = $_POST['edition']['last_pwd'];
		$newpwd = $_POST['edition']['new_pwd'];
		$conf = $_POST['edition']['pwdconf'];
		// Vérification de l'ancien mot de passe 
		$queryLastPwdCheck = sprintf("SELECT Pwd FROM `users` WHERE (Email,Pwd) = ('%s','%s')", $mail,$lastpwd);
		$resLastPwdCheck = requestS($queryLastPwdCheck);
		if (!array_key_exists('error', $resLastPwdCheck)) {
			if (!empty($resLastPwdCheck)) {
				$_SESSION['error']['bad-pwd'] = FALSE;
			} else {
				$_SESSION['error']['bad-pwd'] = TRUE;
			}
		} else {
			print("Problem de query ou de connexion à la bdd");
		}
		// Vérification de la concordance entre le mdp et sa confirmation
		if ($newpwd == $conf) {
			$_SESSION['error']['bad-pwd-conf'] = FALSE;
		} else {
			$_SESSION['error']['bad-pwd-conf'] = TRUE;
		}
		// Modification du mdp
		if (!$_SESSION['error']['bad-pwd'] AND !$_SESSION['error']['bad-pwd-conf']) {
			$queryPwdChange = sprintf("UPDATE `users` SET Pwd = '%s' WHERE Email = '%s'", $newpwd,$mail);
			$resPwdChange = requestTF($queryPwdChange);
			if (empty($resPwdChange)) {
				$_SESSION['error']['pwd-change'] = FALSE;
				if (!empty($link)) {header("Location: $link");}
			} else {
				print("Problem de query ou de connexion à la bdd");
				$_SESSION['error']['pwd-change'] = TRUE;
			}
		} else {
			$_SESSION['error']['pwd-change'] = TRUE;
		}
	}

?>




<?php // fonction d'affichage

	function displayAccount($table,$nbrows) {
		if (isset($_GET['page'])) {
			$actu = $_GET['page'];
		} else {
			$actu =1;
		}
		$offset = ($actu-1)*$nbrows;
		$end = $offset+$nbrows-1;
		$size = count($table);
		if ($end > $size - 1) {$end = $size - 1;}
		print("<table class='account-table'>");
		print("<thead>");
		print("<tr>");
		print("<th> Selection </th>");
		print("<th> Status </th> <th> Name/Institut </th> <th> e-mail </th> <th> Country </th> <th> Password </th>");
		print("<tr>");
		print("</thead>");
		print("<tbody>");
		for ($i = $offset; $i <= $end; $i++) {
			$line = $table[$i];
			$status = $line["User_Cat"];
			$fname = $line["First_Name"];
			$lname = $line["Last_Name"];
			$institut = $line["Institut"];
			if ($fname != NULL AND $lname != NULL) {
				$name_inst = $fname." ".$lname;
				if ($institut != NULL) {
				$name_inst = $name_inst." / ".$institut;
				}
			} else if ($institut != NULL) {
				$name_inst = $institut;
			}
			$mail = $line["Email"];
			$country = $line["Country"];
			$pwd = $line["Pwd"];
			print("<tr>");
			print("<td> <input id='$i' type='checkbox' value='$i' name='indexselection[]' /> </td>");
			print("<input id='$status' type='hidden' value='$status' name='selection[cat][$i]' />");
			print("<input id='$fname' type='hidden' value='$fname' name='selection[first_name][$i]' />");
			print("<input id='$lname' type='hidden' value='$lname' name='selection[last_name][$i]' />");
			print("<input id='$institut' type='hidden' value='$institut' name='selection[institut][$i]' />");
			print("<input id='$mail' type='hidden' value='$mail' name='selection[mail][$i]' />");
			print("<input id='$country' type='hidden' value='$country' name='selection[country][$i]' />");
			print("<input id='$pwd' type='hidden' value='$pwd' name='selection[pwd][$i]' />");
			print("<td> $status </td> <td> $name_inst </td> <td> $mail </td> <td> $country </td> <td> $pwd </td>");
			print("</tr>");
		}
			print("</tbody>");
			print("</table>");
	}

?>