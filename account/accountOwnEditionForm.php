<?php
session_start();
include('accountFunctions.php');
testAndSetCookies();
if (!isset($_SESSION['log'])) {
	$_SESSION['log'] = FALSE;
	header('Location: ../studies/homePage.php');
}
if ($_SESSION['cat'] == 'authority') {
	header('Location: ../studies/homePage.php');
}
?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<link rel="stylesheet" href="../fixstyle.css" />
		<title> ClinicalTrialsByMLG - profile edition </title>
	</head>

	<body>
		<?php
			include('../header.php');
		?>
		<div class='inner-body edit-and-create-account-page' id='edit-profile-page'>

		<section id='edit-profile-section'>
			<fieldset> <legend> Edit the profile </legend>
			<form method="POST" action="accountOwnEditionForm.php" class="form-stlye-1">
				<label for="first_name"> <span> First Name <span class="required">*</span> </span>
				<input id="first_name" type="text" name="edition[first_name]" value=<?php print("'".$_SESSION['first_name']."'"); ?> required/>
				</label>
				<label for="last_name"> <span> Last Name <span class="required">*</span> </span>
				<input id="last_name" type="text" name="edition[last_name]" value=<?php print("'".$_SESSION['last_name']."'"); ?> required/> 
				</label>
				<label for="mail"> <span> e-mail <span class="required">*</span> </span>
				<input id="mail" type="email" name="edition[mail]" value=<?php print("'".$_SESSION['mail']."'"); ?> required/> 
				<?php
					if (isset($_SESSION['error']['email-exists'])) {
						if ($_SESSION['error']['email-exists']) {
							print("<p class='text-error'> This email is already taken </p>");
						}
					}
				?>
				</label>
				<label for="country"> <span> Country <span class="required">*</span> </span>
				<input id="country" type="text" name="edition[country]" value=<?php print("'".$_SESSION['country']."'"); ?> required/>
				</label>
				<div class="form-confirmation">
					<input type='submit' name="edition[btn]" value='Edit' class='submit'/>
					<input type='reset' value='Reset' class='reset'/>
				</div>
			</form>
			</fieldset>
		</section>

		<section id='edit-password-section'>
			<fieldset> <legend> Change the password </legend>
			<form method="POST" action="accountOwnEditionForm.php" class="form-stlye-1">
				<label for="pwd"> <span> Last password <span class="required">*</span> </span>
				<input id="pwd" type="password" name="edition[last_pwd]" minlength='6' required/>
				</label>
				<label for="pwd"> <span> New password <span class="required">*</span>  <span class="additional-info">(6 chars min)</span> </span>
				<input id="pwd" type="password" name="edition[new_pwd]" minlength='6' required/>
				</label>
				<label for="pwdconf"> <span> New password confirmation <span class="required">*</span> </span>
				<input id="pwdconf" type="password" name="edition[pwdconf]" minlength='6' required/>
				<?php 
					if (isset($_SESSION['error']['bad-pwd-conf'])) {
						if ($_SESSION['error']['bad-pwd-conf']) {
							print("<p class='text-error'> Password not coherent </p>");
						} 
					} 
				?>
				</label>
				<div class="form-confirmation">
					<input type='submit' name="edition[btn]" value='Change' class='submit'/>
				</div>
			</form>
			</fieldset>
		</section>



		</div>
		<?php
			include('../footer.php');
		?>	
	</body>
</html>