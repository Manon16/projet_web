<?php
session_start();
include('accountFunctions.php');
testAndSetCookies();
if (!isset($_SESSION['log'])) {
	$_SESSION['log'] = FALSE;
} else {
	if ($_SESSION['log']) {
		header('Location: ../studies/homePage.php'); 
	}
}
if (!isset($_SESSION['cat'])) {
	$_SESSION['cat'] = 'ext';
}
?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<link rel="stylesheet" href="../fixstyle.css" />
		<title> ClinicalTrialsByMLG - log in </title>
	</head>

	<body>
		<?php
			include('../header.php');
			/*echo '<pre>';
			print("</br> SESSION "); print_r($_SESSION);
			print("</br> POST "); print_r($_POST);
			echo '</pre>';*/
		?>
		<div class='inner-body' id='log-page'>

		<!-- il faudra rajouter que quand on sort de la page log In Sign In, les erreurs session sont supprimées -->

		<section class='pop-section'>
			<?php
				if (isset($_SESSION['error']['creation'])) {
					if (!$_SESSION['error']['creation']) {
						print("<div class='success'> Congratulations, you have an account ! You only need to log in. </div>");
					}
				}
				if (isset($_SESSION['error']['deletion'])) {
					if (!$_SESSION['error']['deletion']) {
						print("<div class='success'> Account deleted </div>");
					}
				}
			?>
		</section>

		<div>

		<section id='login-section'>
			<fieldset> <legend> Identification </legend>
				<form method="POST" action="logInSignInForm.php" class="form-stlye-1">
					<?php 
						if (isset($_SESSION['error']['connection'])) {
							if ($_SESSION['error']['connection']) {
								print("<p class='text-error'> Bad e-mail or password </p>"); 
							} 
						} 
					?>
					<label for="mail"> <span> e-mail <span class="required">*</span> </span>
						<input id="mail" type="email" name="connection[mail]" 
							value="<?php if (isset($_POST['connection']) AND $_POST['connection']['btn'] == 'Log In') {print($_POST['connection']['mail']);} ?>" required/> 
					</label>
					<label for="pwd"> <span> Password <span class="required">*</span> </span>
						<input id="pwd" type="password" name="connection[pwd]" 
							value="<?php if (isset($_POST['connection']) AND $_POST['connection']['btn'] == 'Log In') {print($_POST['connection']['pwd']);} ?>" required/> 
					</label>
					<label> 
						<input id="cookie" type="checkbox" name="connection[cookie]"/> 
						<label for="cookie" class="check-label"> Remember me ? <span class="additional-info">(set cookie for 1 week)</span> </label> 
					</label>
					<div class="form-confirmation">
						<input type="submit" name='connection[btn]' value="Log In" class='submit'/>
				</form>
				<form method="POST" action="logInSignInForm.php">
						<input type="submit" name='connection[btn]' value="Reset" class='reset'/>
					</div>
				</form>
			</fieldset>
		</section>



		<section id='signin-section'>
			<fieldset> <legend> Account creation </legend>
				<form method="POST" action="logInSignInForm.php" class="form-stlye-1">
					<label for="first_name"> <span> First Name <span class="required">*</span> </span>
						<input id="first_name" type="text" name="creation[first_name]" 
							value="<?php if (isset($_POST['creation']) AND $_POST['creation']['btn'] != 'Reset') {print($_POST['creation']['first_name']);} ?>" required/>
					</label>
					<label for="last_name"> <span> Last Name <span class="required">*</span> </span>
						<input id="last_name" type="text" name="creation[last_name]" 
							value="<?php if (isset($_POST['creation']) AND $_POST['creation']['btn'] != 'Reset') {print($_POST['creation']['last_name']);} ?>" required/> 
					</label>
					<label for="mail"> <span> e-mail <span class="required">*</span> </span>
						<input id="mail" type="email" name="creation[mail]" 
							value="<?php if (isset($_POST['creation']) AND $_POST['creation']['btn'] != 'Reset') {print($_POST['creation']['mail']);} ?>" required/> 
						<?php
							if (isset($_SESSION['error']['email-exists'])) {
								if ($_SESSION['error']['email-exists']) {
									print("<p class='text-error'> This email is already taken </p>");
								}
							}
						?>
					</label>
					<label for="country"> <span> Country <span class="required">*</span> </span>
						<input id="country" type="text" name="creation[country]" 
							value="<?php if (isset($_POST['creation']) AND $_POST['creation']['btn'] != 'Reset') {print($_POST['creation']['country']);} ?>" required/>
					</label>
					<label for="pwd"> <span> Password <span class="required">*</span> <span class="additional-info">(6 chars min)</span></span>
						<input id="pwd" type="password" name="creation[pwd]" minlength='6' 
							value="<?php if (isset($_POST['creation']) AND $_POST['creation']['btn'] != 'Reset') {print($_POST['creation']['pwd']);} ?>" required/>
					</label>
					<label for="pwdconf"> <span> Password confirmation <span class="required">*</span> </span>
						<input id="pwdconf" type="password" name="creation[pwdconf]" minlength='6' required/>
						<?php 
							if (isset($_SESSION['error']['bad-pwd-conf'])) {
								if ($_SESSION['error']['bad-pwd-conf']) {
									print("<p class='text-error'> Password not coherent </p>");
								} 
							} 
						?>
					</label>
					<div class="form-confirmation">
						<button type="submit" name="creation[btn]" value="create-visitor" class='submit'> Sign In </button>
				</form>
				<form method="POST" action="logInSignInForm.php">
						<input type="submit" name="creation[btn]" value="Reset" class='reset'/>
					</div>
				</form>
			</fieldset>
		</section>

		</div>

		</div>
		<?php
			include('../footer.php');
		?>	
	</body>
</html>
