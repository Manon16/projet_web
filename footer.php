<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<link rel="stylesheet" href="fixstyle.css" />
		<title>  </title>
	</head>
	<body>
		<footer> 
			<ul id='endmenu'>
				<li> <a href="https://www.google.dk/maps/place/Polytech+Nice+Sophia/@43.615556,7.0695895,17z/data=!3m1!4b1!4m5!3m4!1s0x12cc2b1b41c11d4d:0x225231c497224ba5!8m2!3d43.6155521!4d7.0717782?hl=fr" title='Our adress'> Polytech Nice Sophia <br/> 930 Route des Colles <br> 06410 Biot, France </a> </li>
				<li> <a href=../general/contactPage.php> Contact </a> </li>
				<li> <a href=../general/aboutPage.php> About the site </a> </li>
			</ul>
			<div class='logo-container'>
				<img class="logos" src='../logo_poly.png' alt="Logo" title="Logo de Polytech">
				<img class="logos" src='../logo_unice.png' alt="Logo" title="Logo de l'Université de Nice">
			</div>
		</footer>
	</body>
</html>
