<?php
	$current_page = pathinfo($_SERVER['REQUEST_URI'], PATHINFO_FILENAME);
	if ($current_page != 'logInSignInForm') {
		unset($_SESSION['error']['deletion']);
	}
	if ($current_page != 'logInSignInForm' AND $current_page != 'accountCreationFormByAdmin') {
		unset($_SESSION['error']['creation']);
	}
	if ($current_page != 'restrictionForm' AND $current_page != 'studyAndRestrictionPage') {
		unset($_SESSION['error']['restriction-no-user']);
		unset($_SESSION['error']['restriction']);
	}
	if ($current_page != 'studyJsonForm') {
		unset($_SESSION['error']['json-bad-field']);
		unset($_SESSION['error']['json_database']);
	}

?>



<?php /* fonctions pour réaliser des requêtes SQL */

	/* query retourne FALSE en cas d'échec. Pour des requêtes SELECT, SHOW, DESCRIBE ou EXPLAIN réussies, mysqli_query() retournera un objet mysqli_result. Pour les autres types de requêtes ayant réussi, mysqli_query() retournera TRUE. */
	/* fetch_assoc retourne un tableau associatif de chaînes représentant la prochaine ligne dans le jeu de résultats représenté par le paramètre result, où chaque clé du tableau représente le nom d'une colonne du résultat ou NULL s'il n'y a plus de ligne dans le jeu de résultats. */

	/* utilisation de requestS (): tester si le résultat est un array contenant la clé 'error' ou pas */
	function requestS($req) { /* $req = sprintf(de la requete) */
		/* 
		fonction qui ne prend en entrée qu'une requête de type SELECT... 
		et donc qui renvoie un tableau contenant l'erreur en cas d'échec, ou un tableau de dimension 2 (associatif dans numérique) 
		*/
		// Création d'un nouvel objet MYSQLi: connexion à la bdd
		$bdd = new mysqli('localhost','manon','MCP*phpMyAdmin','testweb');
		// Initailisation d'un tableau vide
		$table = array();
		// Vérification de la connexion
		if ($bdd->connect_errno) {
			$table['error'] = $bdd->connect_error;
		}
		// Requête
		$queryRes = $bdd->query($req);
		if ($queryRes == FALSE) { // si échec de la query
			$table['error'] = $bdd->error;
		} else { // si la requete a réussi
			while($row = $queryRes->fetch_assoc()) { 
				array_push($table, $row); // table de la forme: table[1][nomcolonne] ==> si on parcours table, on le fait ligne par ligne
			}
		}
		// Libération du jeu de résultats
		$queryRes->close();
		return($table);
	}

	/* utilisation de requestTF (2): tester si le résultat est vide (pas d'erreur) ou non (erreur: connection ou requête) */
	function requestTF($req) { /* $req = sprintf(de la requete) */
		/* 
		fonction qui ne doit prendre en entrée qu'une requête de type CREATE, INSERT, SET... 
		renvoie une chaine de caractère qui décrit l'erreur survenue lors du dernier appel à une fonction MySQLi, une chaine vide si pas d'erreur 
		*/
		// Création d'un nouvel objet MYSQLi: connexion à la bdd
		$bdd = new mysqli('localhost','manon','MCP*phpMyAdmin','testweb');
		// Vérification de la connexion
		if ($bdd->connect_errno) {return($bdd->connect_error);}
		// Requête
		$queryRes = $bdd->query($req);
		return($bdd->error);
	}

	function testAndSetCookies() {
		if (isset($_COOKIE['mail'])) {
			$_SESSION['log'] = TRUE;
			$_SESSION['idUser'] = $_COOKIE['idUser'];
			$_SESSION['cat'] = $_COOKIE['cat'];
			if (isset($_COOKIE['first_name'])) {$_SESSION['first_name'] = $_COOKIE['first_name'];}
			if (isset($_COOKIE['last_name'])) {$_SESSION['last_name'] = $_COOKIE['last_name'];}
			if (isset($_COOKIE['institut'])) {$_SESSION['institut'] = $_COOKIE['institut'];}
			$_SESSION['mail'] = $_COOKIE['mail'];
			$_SESSION['country'] = $_COOKIE['country'];
		}
		/*echo '<pre>';
		print("</br> SESSION "); print_r($_SESSION);
		print("</br> COOKIE "); print_r($_COOKIE);
		echo '</pre>';*/
	}

	function pagination($table,$nbrows,$link) {
		// cette fonction a besoin de récup une table
		$size = count($table);
		$nbpages = intdiv($size,$nbrows);
		if (isset($_GET['page'])) {
			$actu = $_GET['page'];
		} else {
			$actu = 1;
		}
		if ($size % $nbrows > 0) {$nbpages = $nbpages + 1;}
		if ($size <= $nbrows) {
			$nbpages = 1;
			$pre = 1;
			$post = 1;
		} else {
			switch ($actu) {
			 	case 1:
			 		$pre = $actu;
			 		$post = $actu+1;
			 		break;
			 	case $nbpages:
			 		$pre = $actu-1;
			 		$post = $actu;
			 		break;
			 	default:
			 		$pre = $actu-1;
			 		$post = $actu+1;
			}
		}
		print("<div class='pagination'>");
		print("<a href='".$link."?page=".$pre."'>&laquo;</a>");
		for ($i = 1; $i <= $nbpages; $i++) {
			if ($i == $actu) {$chaine = "<a href='" . $link . "?page=" . $i  . "' class='active'> " . $i . "</a>";}
			else {$chaine = "<a href='" . $link . "?page=" . $i  . "'> " . $i . "</a>";}
			print(" $chaine ");
		}
		print("<a href='".$link."?page=".$post."'>&raquo;</a>");
		print("</div>");
	}

	function studyPreview($line){
		/* Cette fonction permet d'afficher la prévisualisation d'une étude dans un tableau.
		Elle prends en argument la ligne d'un array contenant des études obtenu grâce à la fonction requestS().
		Cette fonction est à placer dans une cellule de tableau, c'est à dire entre <td> et </td>.
		 */
		$idStudy = $line["idStudy"];
		$start_date = $line["Start_Date"];
		$title = $line["Title"];
		if(strlen($title) > 90){
			$title = substr($title,0,90)."...";
		}
		$status = $line["Status"];
		$author="";
		if (empty($line["Author"])){
			$author = "NA";
		}
		else {
			$author = $line["Author"];
		}
		$disease = $line["Disease"];
		$patients_nb = $line["Patient_Nb"];
		$age_min = $line["Age_Min"];
		$age_max = $line["Age_Max"];
		$abstract  = $line["Abstract"];
		if(strlen($abstract) > 90){
			$abstract = substr($abstract,0,90)."...";
		}

		echo "<a href= ../studies/oneStudyPage.php?idStudy=".$idStudy.">".$start_date." - ".$title."</a> <br>"; 
		echo "Status: ".$status." - Author: ".$author." - Disease: ".$disease."<br>";
		echo $patients_nb." patients - from ".$age_min." to ".$age_max." years old <br>";
		echo "Abstract: ".$abstract;
	}


?>

