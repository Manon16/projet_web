<?php 
	$logged = $_SESSION['log'];
	$usercat = $_SESSION['cat'];
?>


<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<link rel="stylesheet" href="fixstyle.css" />
		<title>  </title>
	</head>
	<body>
		<header> <!-- header fixe ou pas ? -->
			<nav>
				<div class='logo-container'>
					<a href='../studies/homePage.php'> <img class='logos' src='../logo_site.jpg' alt="Logo" title="Home"> </a>
				</div>
				<ul id='menu'>
					<li> <a href='../studies/homePage.php' class='menu-link'> Home </a> </li>
					<?php
						if ($logged) {
							switch ($usercat) {
								case 'visitor':
									print("<li> <a href=../studies/bookmarkManagement.php class='menu-link'> Bookmarks </a> </li>");
									break;
								case 'sponsor':
									print(
										"<li> <a href=../studies/bookmarkManagement.php class='menu-link'> Bookmarks </a> </li>
										<li> <a href=../studyManagement/studyManagement.php class='menu-link'> My Studies </a> </li>");
									break;
								case 'moderator':
									print(
										"<li> <a href=../studyManagement/studyManagement.php class='menu-link'> Restriction Management </a> </li>
										<li> <a href=../reports/reportPage.php class='menu-link'> Reports </a> </li>");
									break;
								case 'administrator':
									print(
										"<li> <a href=../studyManagement/studyManagement.php class='menu-link'> Restriction Management </a> </li>
										<li> <a href=../account/accountManagement.php class='menu-link'> Account Management </a> </li>
										<li> <a href=../reports/reportPage.php class='menu-link'> Reports </a> </li>");
									break;
								case 'authority':
									print(
										"<li> <a href=../studies/bookmarkManagement.php class='menu-link'> Bookmarks </a> </li>
										<li> <a href=../reports/reportPage.php class='menu-link'> Reports </a> </li>");
									break;
							}
							if ($usercat == 'authority') {
								$profile_name = $_SESSION['institut'];
							} else {
								$profile_name = $_SESSION['first_name']." ".$_SESSION['last_name'];
							} print(
								"<li class='dropdown'> <a href='javascript:void(0)' class='menu-link'> ".$profile_name." </a>
									<div class='dropdown-content'>
									<a href='../account/profilePage.php' class='menu-link'> Profile </a>
									<a href='../account/logInSignInForm.php'> <form method='POST' action='../account/logInSignInForm.php'> <input type='submit' name='connection[btn]' value='Log Out' id='logout-btn'/> </form> </a>
									</div>
								</li>");
						} else {
							print("<li> <a href='../account/logInSignInForm.php' class='menu-link'> Log In </a> </li>");
						}
					?>	
				</ul>
			</nav>
		</header>
	</body>
</html>
