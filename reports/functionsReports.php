<?php
include('../functions.php');
require('fpdf/fpdf.php');


// Load all the date from the database


function statData(){
//Count total
	$req = "SELECT COUNT(*) as total FROM studies";
	$data=requestS($req);
	$total = $data[0]['total'];
//Count studies validated
	$req = "SELECT COUNT(*) as valid FROM Studies WHERE Status='validated'";
	$data=requestS($req);
	$validated = $data[0]['valid'];
//Count studies refused
	$req = "SELECT COUNT(*) as ref FROM studies WHERE Status='refused'";
	$data=requestS($req);
	$refused = $data[0]['ref'];
//Count studies ongoing
	$req = "SELECT COUNT(*) as ong FROM studies WHERE Status='ongoing'";
	$data=requestS($req);
	$ongoing = $data[0]['ong'];
//Count studies planified
	$req = "SELECT COUNT(*) as plan FROM studies WHERE Status='planified'";
	$data=requestS($req);
	$planified = $data[0]['plan'];
//Count studies gender male
	$req = "SELECT COUNT(*) as M FROM studies WHERE Gender='Male'";
	$data=requestS($req);
	$male = $data[0]['M'];
	//Count studies gender female
	$req = "SELECT COUNT(*) as F FROM studies WHERE Gender='Female'";
	$data=requestS($req);
	$female = $data[0]['F'];
	//Count studies gender both
	$req = "SELECT COUNT(*) as MF FROM studies WHERE Gender='Both'";
	$data=requestS($req);
	$both = $data[0]['MF'];
//Count studies patient nb between 0 and 50
	$req = "SELECT COUNT(*) as cat1 FROM studies WHERE Patient_Nb<=50";
	$data=requestS($req);
	$Nb_0_50 = $data[0]['cat1'];
	//Count studies patient nb between 50 and 250
	$req = "SELECT COUNT(*) as cat2 FROM studies WHERE Patient_Nb<=250 and Patient_Nb>50";
	$data=requestS($req);
	$Nb_50_250 = $data[0]['cat2'];
//Count studies patient nb between 250 and 1000
	$req = "SELECT COUNT(*) as cat3 FROM studies WHERE Patient_Nb<=1000 and Patient_Nb>250";
	$data=requestS($req);
	$Nb_250_1000 = $data[0]['cat3'];
//Count studies patient nb more than 1000
	$req = "SELECT COUNT(*) as cat4 FROM studies WHERE Patient_Nb>1000";
	$data=requestS($req);
	$Nb_more1000 = $data[0]['cat4'];


	$reqInsert="INSERT INTO reports (studyTotal,studyValidated,studyRefused, studyOngoing, studyPlanified, male, female, male_female, cat1, cat2, cat3, cat4) VALUES (".$total.",".$validated.",".$refused.",".$ongoing.",".$planified.",".$male.",".$female.",".$both.",".$Nb_0_50.",".$Nb_50_250.",".$Nb_250_1000.",".$Nb_more1000.")";
	$res = requestTF($reqInsert);
}

function create_pdf($idReport){
 	$pdf= new FPDF();
 	$pdf->AddPage();
 	$pdf->SetFont('Arial','B',16);
 	$pdf->Image('../logo_site.jpg',10,6,30);
 	$pdf ->Cell(0,10,"Report of ".$idReport,0,1,'C');
 	$pdf ->Cell(100,10,"Studies total at ".$idReport,0,0,'C');
 	$pdf->Image('graphes/total.png',10,30,100);
 	$pdf ->Cell(100,10,"Studies status at ".$idReport,0,1,'C');
 	$pdf->Image('graphes/status.png',110,30,100);
 	$pdf->Ln(80);
 	$pdf ->Cell(100,10,"Evolution of status",0,0,'C');
 	$pdf->Image('graphes/status_evol.png',10,120,100);
 	$pdf ->Cell(100,10,"Gender repartition at ".$idReport,0,1,'C');
 	$pdf->Image('graphes/gender.png',110,120,100);
	$pdf->Ln(80);
 	$pdf ->Cell(100,10,"Patients number repartition at ".$idReport,0,1,'C');
 	$pdf->Image('graphes/patients.png',10,210,100);
 	$pdf->Output('F',"pdf_file/".$idReport.".pdf");
}

function display_pdf(){
	$req="SELECT dateReport FROM reports ORDER BY dateReport DESC ";
	$res=requestS($req);
	foreach ($res as $key => $value) {
		print("<a href= 'reportView.php?date=".$value['dateReport']."'> Report of ".$value['dateReport']." </a>");
	}
}

 ?>

