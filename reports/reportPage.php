<?php
session_start();
include('functionsReports.php');

testAndSetCookies();
if (!isset($_SESSION['log'])) {
	$_SESSION['log'] = FALSE;
	header('Location: ../studies/homePage.php');
}
if ($_SESSION['cat'] != 'administrator' AND $_SESSION['cat'] != 'moderator' AND $_SESSION['cat'] != 'authority') {
	print($_SESSION['cat']);
	header('Location: ../studies/homePage.php');
}

?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<link rel="stylesheet" href="../fixstyle.css" />
		<title> Report page </title>
	</head>
	<body>
		<?php
			include('../header.php');
		?>
		<div class='inner-body' id="reports-page">

		<?php if ($_SESSION['cat']=='administrator') { ?>
		<section id='report-create-section'>
			<div class="form-confirmation">
			<form method="post" action="scriptReport.php">
				<input type="submit" name="Create a new report" value="Create" class='submit'>
			</form>
			</div>
		</section>
		<?php } ?>

		<section id='reports-section'>

				<?php display_pdf(); ?>

		</section>

		</div>
		<?php
			include('../footer.php');
		?>	
	</body>
</html>