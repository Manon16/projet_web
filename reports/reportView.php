<?php
session_start();
include('functionsReports.php');
testAndSetCookies();
if (!isset($_SESSION['log'])) {
	$_SESSION['log'] = FALSE;
	header('Location: ../studies/homePage.php');
}
if ($_SESSION['cat'] != 'administrator' AND $_SESSION['cat'] != 'moderator' AND $_SESSION['cat'] != 'authority') {
	print($_SESSION['cat']);
	header('Location: ../studies/homePage.php');
}
?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<link rel="stylesheet" href="../fixstyle.css" />
		<title> Report View </title>
	</head>
	<body>
		<?php
			//include('../header.php');
		?>
		<div class='inner-body centered' id="report-page">

		 <article>
		 	<iframe id="pdfReport" src="pdf_file/<?php print($_GET['date']);?>.pdf"></iframe>
		 </article>

		</div>
		<?php
			//include('../footer.php');
		?>	
	</body>
</html>
