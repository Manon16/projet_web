<?php
session_start();
include('studiesFunctions.php');
testAndSetCookies();
if (!isset($_SESSION['log'])) {
	$_SESSION['log'] = FALSE;
	header('Location: ../studies/homePage.php');
}
if ($_SESSION['cat'] != 'visitor' AND $_SESSION['cat'] != 'sponsor' AND $_SESSION['cat'] != 'authority') {
	header('Location: ../studies/homePage.php');
}
?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<link rel="stylesheet" href="../fixstyle.css" />
      <title> ClinicalTrialsByMLG - Home </title>
	</head>
	
    <body>

		<?php 
			include('../header.php'); 
		?>
		<div class='inner-body' id='bookmark-page'>

		<section>
			<h1> <span>★</span> My bookmarks <span>★</span> </h1>
		</section>

		<section class='pop-section'>
			<?php
				/* Suppression des favoris */
				if(isset($_POST['bookmarks']['btn'])){ 
					if(isset($_POST['bookmarks']['selection'])) {
						foreach($_POST['bookmarks']['selection'] as $bookmark) {
							deleteBookmark($bookmark,$_SESSION['idUser']);
						}
					} else {
						print("<div class='failure'> No bookmark selected </div>");
					}
				}
			?>
		</section>

		<section id='bookmark-section'>
			<?php
				/* Affichage des études */
				$nbrows = 4; 
				$table_bookmarks = requestS("SELECT s.* FROM studies s INNER JOIN bookmarks b ON s.idStudy = b.idStudy WHERE b.idUser = ".$_SESSION['idUser']);
				displayBookmarks($table_bookmarks,$nbrows);

				pagination($table_bookmarks,$nbrows,"bookmarkManagement.php");
			?>
		</section>

		</div>
		<?php
			include('../footer.php');
		?>

    </body>


</html>