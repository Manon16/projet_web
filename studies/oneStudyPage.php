
<?php
session_start();
include('studiesFunctions.php');
testAndSetCookies();
if (!isset($_SESSION['log'])) {
	$_SESSION['log'] = FALSE;
}
if (!isset($_SESSION['cat'])) {
	$_SESSION['cat'] = 'ext';
}

$idStudy = $_GET['idStudy'];
$study_information = requestS("SELECT * FROM studies WHERE idStudy = '$idStudy'");
$study_information=$study_information[0];
$current_page = $_SERVER['REQUEST_URI'];
$page = pathinfo($_SERVER['REQUEST_URI'], PATHINFO_FILENAME);
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<link rel="stylesheet" href="../fixstyle.css" />
		<title> One Study Page </title>
	</head>
	<body>
		<?php include('../header.php'); ?>
		<div class='inner-body study-page' id='study-page'>

			<div class='row-div'>	
			<?php include('../studies/studyPart.php'); ?>
			</div>

		</div>
		<?php
			include('../footer.php');
		?>	
	</body>
</html>
