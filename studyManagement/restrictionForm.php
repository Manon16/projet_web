<?php
session_start();
include('studyManagementFunctions.php');
testAndSetCookies();
if (!isset($_SESSION['log'])) {
	$_SESSION['log'] = FALSE;
	header('Location: ../studies/homePage.php');
}
if ($_SESSION['cat'] != 'sponsor') {
	header('Location: ../studies/homePage.php');
}

if (isset($_POST['idsStudy'])) {
	$studyList = $_POST['idsStudy'];
} else if (isset($_POST["new-restriction"]["idsStudy"])) {
	$studyList =  $_POST["new-restriction"]["idsStudy"];
}
if (isset($_SESSION['restriction']['idStudies'])) {
	$studyList = $_SESSION['restriction']['idStudies'];
	unset($_SESSION['restriction']['idStudies']);
}


?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<link rel="stylesheet" href="../fixstyle.css" />
		<title> Add restiction </title>
	</head>

	<body>
	<?php include('../header.php'); ?>
	<div class='inner-body centered' id="restriction-page">


		<section class='pop-section'>
			<?php
				if (isset($_SESSION['error']['restriction-no-user']) AND $_SESSION['error']['restriction-no-user']) {
					print("<div class='failure'> No email in our customers </div>");
				}
			?>
		</section>


		<section id="restriction-demand-title-section">
			<h1> Add a restriction list to your(s) study(ies) </h1>
			<ul>
			<?php
				//récupération de l'id et du titre de(s) étude(s)
				foreach ($studyList as $key => $idStudy) {
					$title_res=requestS("SELECT Title FROM studies WHERE idStudy = '$idStudy'");
					$study_title=$title_res[0]['Title'];
					print("	<li> Title: $study_title </li> <ul> <li> ID: $idStudy </li> </ul> ");
				}
			?>
			</ul>
		</section>

		<section id="restriction-demand-section"> 
			<form method = "POST" action="restrictionForm.php" class="form-style-2">
				
				<?php 
					foreach ($studyList as $key => $idStudy) {
						print("<input type='hidden' name='new-restriction[idsStudy][]' value='".$idStudy."' />");
					}
				?>

				<label for="ListeEmail"> <span> List of emails separated with semicolons <span class="required">*</span> </span> </label>
				<textarea id="ListeEmail" rows="3" cols="50" name="new-restriction[listeEmail]" 
					placeholder="email1@example.com;email2@example.com;email3@example.com" required></textarea> 
				
				<label for="Justification"> <span> Justification <span class="required">*</span> </span> </label>
				<textarea id="Justification" name="new-restriction[Justification]" rows="5" cols="50" 
					placeholder="Only these people should have access to this study because..." required></textarea> 
				
				
				
					<button type="submit" name="new-restriction[btn]" value="new" class='submit'> Send your restriction request </button>
				

			</form>
		</section>

		</div>
		<?php include('../footer.php');?>
	</body>
</html>

