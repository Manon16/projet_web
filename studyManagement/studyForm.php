<?php
session_start();
if (!isset($_SESSION['log'])) {
	$_SESSION['log'] = FALSE;
	header('Location: ../studies/homePage.php');
}
if ($_SESSION['cat'] != 'sponsor') {
	header('Location: ../studies/homePage.php');
}
include('studyManagementFunctions.php');
$dose_units=array('µg','mg','g','µg/mL','mg/mL','g/L','µg/kg','mg/kg','g/kg','mg/min','mg/h','g/h','mL/h','ppm',);
$administration_routes=array('oral','cutaneous','injection','respiratory','occulary','other');
?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<link rel="stylesheet" href="../fixstyle.css" />
		<title> ClinicalTrialsByMLG - Study submission form </title>
		
	</head>
	<body>
	<?php	include('../header.php');	?>
	<div class='inner-body study-form-page' id="study-form-page">

	<section id="study-form-title-section" class="study-form-title-section">
		
		<h1> Study submission </h1>
		<a href='studyJsonForm.php' > Studies submission with Json File </a>

	</section>

	<section id='study-form-section'>
		<form method="POST" action="studyForm.php" enctype="multipart/form-data" class="form-stlye-1"> 
		
		<fieldset id="study-fieldset"> <legend> Study </legend>
			
			<label for="idStudy"> <span> International ID <span class="required">*</span> </span>
			<input id="idStudy" name="idStudy" required/> 
				<?php 
					if (isset($error['uniqueID'])) {
							print("<p class='text-error'>".$error['uniqueID']."</p>");
					} 
				?>
			</label>

			<label> <span> Trial Status <span class="required">*</span> : </span>
				<label class="check-label"><input name="Status" type="radio" value="planified" required/> planified</label>
				<label class="check-label"><input name="Status" type="radio" value="ongoing"/> ongoing</label>
				<label class="check-label"><input name="Status" type="radio" value="validated"/> validated</label>
				<label class="check-label"><input name="Status" type="radio" value="refused"/> refused</label>
			</label>

			<label for="Start_Date"> <span> Start date <span class="required">*</span> </span>
			<input id="Start_Date" type="date" name="Start_Date" required/></label>

			<label for="End_Date"> <span> End date </span>
			<input id="End_Date" type="date" name="End_Date"/></label>

			<label for="Title"> <span> Title <span class="required">*</span> </span>
			<input id="Title" name="Title" required/></label>

			<label class="location"> <span> Fill at least one location <span class="required">*</span> :</span><br>

			<ul>
			<li><label for="Institut" class="location"> <span> Institute </span> 
				<input id="Institut"  name="Institut"/>
			</label></li> 

			<li><label for="Country" class="location"> <span> Adress </span> 
				<input id="Country" name="Country" placeholder="Country" />
				<input id="Nb_Street" name="Nb_Street" placeholder="City, Street, Number" />
			</label></li>
			</ul>
				<?php 
					if (isset($error['Location'])) {
							print("<p class='text-error'>".$error['Location']."</p>");
					} 
				?>
	
			</label>

			<label > <span> Trial phase <span class="required">*</span> : </span> 
				<label class="check-label"><input  type="radio" name="Phase" value="0" required/>0</label>
				<label class="check-label"><input  type="radio" name="Phase" value="1"/>1</label>
				<label class="check-label"><input  type="radio" name="Phase" value="2"/>2</label>
				<label class="check-label"><input  type="radio" name="Phase" value="3"/>3</label>
				<label class="check-label"><input  type="radio" name="Phase" value="4"/>4</label>
				<label class="check-label"><input  type="radio" name="Phase" value="Not applicable"/>Not applicable</label>
			</label>

			<label for="Author"> <span> Author </span>
			<input id="Author"  name="Author"/></label>

			<label for="Weblink"> <span> Trial weblink <span class="required">*</span> </span> 
			<input id="Weblink" type="url" name="Weblink" required/></label>

		</fieldset>

		<fieldset id="disease-fieldset"> <legend> Disease </legend>

			<label for="Disease"> <span> Disease name <span class="required">*</span> </span> 
			<input id="Disease" name="Disease" required/></label>

			<label for="Disease_Type"> <span> Disease type <span class="required">*</span> </span> 
			<select id="Disease_Type" name="Disease_Type" required>
					<option value="infectious and parasitic diseases">infectious and parasitic diseases</option>
					<option value="neoplasms">neoplasms</option>
					<option value="endocrine nutritional and metabolic diseases and immunity disorders">endocrine nutritional and metabolic diseases <br> and immunity disorders</option>
					<option value="diseases of the blood and blood-forming organs">diseases of the blood and blood-forming organs</option>
					<option value="mental disorders">mental disorders</option>
					<option value="diseases of the nervous system and sense organs">diseases of the nervous system and sense organs</option>
					<option value="diseases of the circulatory system">diseases of the circulatory system</option>
					<option value="diseases of the respiratory system">diseases of the respiratory system</option>
					<option value="diseases of the digestive system">diseases of the digestive system</option>
					<option value="diseases of the genitourinary system">diseases of the genitourinary system</option>
					<option value="complications of pregnancy childbirth and the puerperium">complications of pregnancy childbirth and the puerperium</option>
					<option value="diseases of the skin and subcutaneous tissue">diseases of the skin and subcutaneous tissue</option>
					<option value="diseases of the musculoskeletal system and connective tissue">diseases of the musculoskeletal system and connective tissue</option>
					<option value="congenital anomalies">congenital anomalies</option>
					<option value="certain conditions originating in the perinatal period">certain conditions originating in the perinatal period</option>
					<option value="symptoms signs and ill-defined conditions">symptoms signs and ill-defined conditions</option>
					<option value="injury and poisoning">injury and poisoning</option>
					<option value="other">other</option>
				</select>
			</label>

			<label for="Disease_Stage"> <span> Disease Stage <span class="required">*</span> </span> 
			<select id="Disease_Stage" name="Disease_Stage" required>
				<option value="early">early</option>
				<option value="flare-up">flare-up</option>
				<option value="progressive">progressive</option>
				<option value="refractory">refractory</option>
				<option value="acute">acute</option>
				<option value="chronic">chronic</option>
				<option value="clinical">clinical</option>
				<option value="subclinical">subclinical</option>
				<option value="cure">cure</option>
				<option value="terminal">terminal</option>
				<option value="other">other</option>
			</select>
			</label>
		</fieldset>

		<fieldset id="demographic-fieldset"> <legend> Demographic data </legend>

			<label for="Age_Min"> <span> Patient's minimum age <span class="required">*</span> </span> 
			<input id="Age_Min" type="number" name="Age_Min" min='0' required/></label>

			<label for="Age_Max"> <span> Patient's maximum age <span class="required">*</span> </span> 
			<input id="Age_Max" type="number" name="Age_Max" min='0' required/></label>

			<label> <span> Patients gender <span class="required">*</span> : </span> 
				<label class="check-label"><input type="radio" name="gender" value="Male" required/>Male</label>
				<label class="check-label"><input type="radio" name="gender" value="Female"/>Female</label>
				<label class="check-label"><input type="radio" name="gender" value="Both"/>Both</label>
			</label>

			<label for="Patient_Nb"> <span> Number of people enrolled <span class="required">*</span> </span> 
			<input id="Patient_Nb" type="number" name="Patient_Nb" min='0' max='10000000000' required/></label>

		</fieldset>

		<fieldset id="treatment-fieldset"> <legend> Treatment </legend>

			<label for="Treatment_mol"> <span> Treatment (molecule) <span class="required">*</span> </span> 
			<input id="Treatment_mol" name="Treatment_Mol" required/></label>

			<label for="Treatment_target"> <span> Treatment's target </span>
			<input id="Treatment_target" type="Treatment_target" name="Treatment_Target"/></label>

			<div id="administration-table-div"> 

			<label> <span> 1st option of administration <span class="additional-info">(mandatory)</span> </span>
				<label for="Admin1" class="check-label"> <span class="check-label"> Route of administration <span class="required">*</span> </span>
				<select name="Admin1" id="Admin1">
					<?php foreach($administration_routes as $route){
						print("<option value=$route>$route</option>");
					} ?>
				</select> </label>
				<label for="Dose_Min1" class="check-label"> Minimal dose 
				<input id="Dose_Min1" type="number" name="Dose_Min1"/></label>
				<label for="Dose_Max1" class="check-label"> Maximal dose 
				<input id="Dose_Max1" type="number" name="Dose_Max1"/></label>
				<label for="Unit1" class="check-label"> Units's dose 
				<select name="Unit1" id="Unit1">
					<?php foreach($dose_units as $dose_unit){
						print("<option value=$dose_unit>$dose_unit</option>");
					} ?>
					<option value ='' selected>---</option>
				</select></label>
			</label>

			<label> <span> 2nd option of administration <span class="additional-info">(optionnal)</span> </span>
				<label for="Admin2" class="check-label"> Route of administration
				<select name="Admin2" id="Admin2">
					<?php foreach($administration_routes as $route){
						print("<option value=$route>$route</option>");
					} ?>
					<option value ='' selected>---</option>
				</select> </label>
				<label for="Dose_Min2" class="check-label"> Minimal dose
				<input id="Dose_Min2" type="number" name="Dose_Min2"/></label>
				<label for="Dose_Max2" class="check-label"> Maximal dose
				<input id="Dose_Max2" type="number" name="Dose_Max2"/></label>
				<label for="Unit2" class="check-label"> Units's dose
				<select name="Unit2" id="Unit2">
					<?php foreach($dose_units as $dose_unit){
						print("<option value=$dose_unit>$dose_unit</option>");
					} ?>
					<option value ='' selected>---</option>
				</select></label>
			</label>

			<label> <span> 3rd option of administration <span class="additional-info">(optionnal)</span> </span>
				<label for="Admin3" class="check-label"> Route of administration
				<select name="Admin3" id="Admin3">
					<?php foreach($administration_routes as $route){
						print("<option value=$route>$route</option>");
					} ?>
					<option value ='' selected>---</option>
				</select> </label>
				<label for="Dose_Min3" class="check-label"> Minimal dose
				<input id="Dose_Min3" type="number" name="Dose_Min3"/></label>
				<label for="Dose_Max3" class="check-label"> Maximal dose
				<input id="Dose_Max3" type="number" name="Dose_Max3"/></label>
				<label for="Unit3" class="check-label"> Units's dose
				<select name="Unit3" id="Unit3">
					<?php foreach($dose_units as $dose_unit){
						print("<option value=$dose_unit>$dose_unit</option>");
					} ?>
					<option value ='' selected>---</option>
				</select></label>
			</label>

			</div>

		</fieldset>

		<fieldset id="general-fieldset"> <legend> General </legend>

			<label for="study_pdf"> <span> Study file<span class="required">*</span> <span class="additional-info">(pdf only, max size: 10MB)</span> </span> 
			<input id="study_pdf" name="study_pdf" type="file" accept="application/pdf" required>
				<?php 
					if (isset($error['file'])) {
							print("<p class='text-error'>".$error['file']."</p>");
					} 
				?>
			</label>

			<label for="Abstract"> <span> Abstract <span class="required">*</span> </span> 
			<textarea id="Abstract" name="Abstract" cols="40" rows="5" style="vertical-align:top" required></textarea></label>

		</fieldset>
		<fieldset id="restriction-fieldset"> <legend> Restriction </legend>

			<label> <span> Add a restriction list ? <span class="required">*</span> </span> 
				<label class="check-label"><input name="restriction" type="radio" value="Yes" required/>Yes</label>
				<label class="check-label"><input name="restriction" type="radio" value="No"/>No</label>
			</label>

		</fieldset>
			<span> *required fields</span>
			<div class="form-confirmation">
				<input type="Submit" name="studyFormSubmit" value="Submit" class="submit">
			</div>
			
		</form>
	</section>

	</div>
	<?php include('../footer.php');?>
	</body>
	

</html>

