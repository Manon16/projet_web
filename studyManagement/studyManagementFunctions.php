<?php 
 	include('../functions.php');
?>

<?php
	
	// test pour restrictionForm: soumission d'une nouvelle restriction par sponsor
	if(isset($_POST["new-restriction"]["btn"])){
		unset($_POST["new-restriction"]["btn"]);//On enlève la valeur du submit qui n'est pas utile pour la BDD ??
		addNewRestriction('studyManagement.php');
	}

	if(isset($_POST["edit-restriction"]["btn"])){
		unset($_POST["edit-restriction"]["btn"]);//On enlève la valeur du submit qui n'est pas utile pour la BDD ??
		retryRestriction('studyManagement.php');
	}

	if(isset($_POST["studyFormSubmit"])){	
		/*Gestion des erreurs*/
		//Vérification si l'ID n'existe pas déjà dans la BDD.
		$error=array();
		$res_unique_id = requestS("SELECT 1 FROM studies WHERE idStudy = '".$_POST['idStudy']."'");
		if(!empty($res_unique_id)){
			$error['uniqueID']="This international trial ID is already taken.";
		}
		//Si l'on a pas au moins 1 des 2 options de localisation qui est renseigné...
		if(empty($_POST['Institut']) && (empty($_POST['Country']) || empty($_POST['Nb_Street']))){ 
			$error['Location']="Fill at least one location.";
		} //...alors on renvoie un message d'erreur 

		/*upload fichier pdf*/
		
		 
		if(is_uploaded_file($_FILES["study_pdf"]["tmp_name"])){ 
			if ($_FILES["study_pdf"]["error"] > 0){
					$error['file'] = "Error during file upload";
				}
			if ($_FILES["study_pdf"]["size"] > 10 * 1048576){ 
					$error['file'] = "The size of the pdf exceeds 10 MB.";
				}
			else{
				if(empty($error)){ //S'il n'y a pas d'erreur plus haut, alors  on enregistre le fichier
					$pdf_path=$_SERVER['DOCUMENT_ROOT']."/projet_web/studiesPDF/".$_POST['idStudy'].".pdf";
					move_uploaded_file($_FILES["study_pdf"]["tmp_name"] ,$pdf_path);
				}
			}
		}
		/*Ajout des informations dans $study_information */
		if(empty($error)){
			$restriction_answer=$_POST["restriction"];
			unset($_POST["restriction"]);
			$_SESSION['restriction']['idStudies']=array($_POST["idStudy"]);
			unset($_POST["studyFormSubmit"]);//On enlève la valeur du submit qui n'est pas utile pour la BDD
			$study_information=$_POST; //On récupère les données de POST
			$study_information['idUser'] = $_SESSION['idUser']; //On y ajoute: -l'id du commanditaire
			$study_information['Sub_Date']=date("Y-m-d"); //-La date de soumission
			if(!empty($study_information['End_Date'])){ 
				$end_date=strtotime($study_information['End_Date']);
				$start_date=strtotime($study_information['Start_Date']);
				$study_information['Duration']=($end_date-$start_date)/60/60/24; //-La durée en jours		
			}
			insertInDB($study_information,"studies");
			if($restriction_answer == "Yes"){
				header("Location: restrictionForm.php");
			}
			if($restriction_answer == "No"){
				header("Location: studyManagement.php");
			}
		}
	}

	//Gestion de la réponse du modérateur par rapport à une restriction d'étude
	if(isset($_POST['acceptedRestriction'])){
		requestTF("UPDATE restrictions SET Restriction_Status = 'accepted' WHERE idRestriction = '".$_POST['idRestriction']."'");
	}
	if(isset($_POST['refusedRestriction'])){
		requestTF("UPDATE restrictions SET Restriction_Status = 'refused' WHERE idRestriction = '".$_POST['idRestriction']."'");
		requestTF("UPDATE restrictions SET Response = '".$_POST['response']."' WHERE idRestriction = '".$_POST['idRestriction']."'");
	}
	if (isset($_POST['studyFormUpdate'])){
		unset($_POST['studyFormUpdate']);
		updateDB($_POST);
	}
?>

<?php 
//This part contains several fonctions link to the study management 

/* Cette fonction prends en entrée :
		- La variable $_POST du formulaire des études
		- Une variable $study qui correspond au resultat de la requete sql SELECT
	Dans cette fonction, on test quels sont les champs qui ont été modifié par le commanditaire en comparant les valeurs issue des deux variables d'entrée. Les champs différents sont enregistré, puis la variable set est générée par itération sur les champs/valeur enregistré.
	Pour finir on réalise une requete UPDATE sur les tabels de la bdd
*/
function updateDB($post){
	$query = "UPDATE studies SET ";
	foreach ($post as $key => $value) {
		if ($key != "idStudy"){
			$query=$query.$key."='".$value."',";
		}
	}
	$query=substr($query,0,-1);
	$query = $query." WHERE idStudy='".$post['idStudy']."'";
	//echo($query);
	$res = requestTF($query);
	//echo $res;
}


/* Cette fonction prends en entrée un array de type $_POST et une variable $table correspondant a la table a modifier
	Le but de la fonction est de récupérer les valeurs des différentes colonne du $_POST.
	La fonction supprime les valeurs de $_POST égale a une valeur "". 
	Une fois le $_POST netoyé on INSERT  dans la table souhaitée. 
*/ 
function insertInDB($array, $table){
//Netoyage de $_POST
	$column = "";
	$values = "";
	foreach ($array as $key => $value) {
		if ($value == "") {
			unset($array[$key]);}
		else {
			$column = $column.$key.",";
			$values = $values."'".$value."',";}
	}
// Remplacement le dernier charactere "," par ""
		$column = substr($column,0,-1);
		$values = substr($values,0,-1);
//Construciton de la requete
	$query = "INSERT INTO $table($column) VALUES($values)";
	$res = requestTF($query);
	return $res;
}


/*
This function aims to display all the informations about the sponsor's studies
*/
function studiesDisplay($table,$nbrows){
	if (isset($_GET['page'])) {
		$actu = $_GET['page'];
	} else {
		$actu =1;
	}
	$offset = ($actu-1)*$nbrows;
	$end = $offset+$nbrows-1;
	$size = count($table);
	if ($end > $size - 1) {$end = $size - 1;}
	if (count($table) == 0) {
		if ($_SESSION['cat']=='sponsor') {
			print("<section class='pop-section'><div class='dialog'><span> You have no study yet </span></div></section>");
		} else if ($_SESSION['cat']=='administrator' OR $_SESSION['cat']=='moderator') {
			print("<section class='pop-section'><div class='dialog'><span> There is no restriction yet </span></div></section>");
		}
		
	} else {
		print("<table class='studies-table'>");
		if ($_SESSION['cat']=='sponsor') {
			print("<thead> <th> Submission date </th> <th> Status </th> <th> Studies </th> <th> Restriction status </th> </thead>");
		} else if ($_SESSION['cat']=='administrator' OR $_SESSION['cat']=='moderator') {
			print("<thead> <th> Submission date </th> <th> Sponsor </th> <th> Studies </th> <th> Restriction status </th> </thead>");  
		}
		print("<tbody>");
		//Foreach study(ligne) in the array
		for ($key = $offset; $key <= $end; $key++) {
			print("<tr>");
			//column sub_date
				print("<td>");
					print($table[$key]['Sub_Date']);
				print("</td>");
			// Column sponsor
				if ($_SESSION['cat']=='administrator' OR $_SESSION['cat']=='moderator') {
				print("<td>");
					print($table[$key]['Institut']);
				print("</td>");
				}
			//Column status
				else if ($_SESSION['cat']=='sponsor') {
				print("<td>");
					print($table[$key]['Status']."<br>");
					print("<form method='POST' action='editStudy.php'>");
					print("<button name='idStudy' value=".$table[$key]['idStudy']." type='submit' class='select'> Edit the Study </button>");
					print("</form>");
				print("</td>");
				}
			//Column preview + radiobutton
				print("<td class='study-cell'>");
					print(studyPreview($table[$key]));
				print("</td>");
			//column restriction
				//Check the idRestriction
				// if no id => purpose to add restriction
				if ($_SESSION['cat']=='sponsor' AND empty($table[$key]['idRestriction'])){
					print("<td>");
						print("<form method='POST' action='restrictionForm.php'>");
						print("<button name='idsStudy[]' value=".$table[$key]['idStudy']." type='submit' class='select'> Add a Restriction </button>");
						print("</form>");
					print("</td>");
				}
				//Else, print the restriction status
				else{
					$req="SELECT Restriction_Status as status FROM restrictions WHERE idRestriction=".$table[$key]['idRestriction'];
					$result=requestS($req);
					$status = $result[0]['status'];
					print("<td>");
					switch ($status) {
						case 'refused': print("<span class='refused'> Refused </span>"); break;
						case 'accepted': print("<span class='accepted'> Accepted </span>"); break;
						case 'not treated yet': print("<span class='not_treated'> Not treated yet </span>"); break;
					}
						print("<form method='POST' action='studyAndRestrictionPage.php'>");
						print("<button name='idsStudy[]' value=".$table[$key]['idStudy']." type='submit' class='select'> Check Restriction </button>");
						print("</form>");
					print("</td>");
				}
			print("</tr>");
		}
		print("</tbody>");
		print("</table>");
	}
}

function userEmailSplit($array) {
	//Prétraitement pour récupérer les idUsers en une seule requête.
	$emails_array=explode(';',$array["listeEmail"]);
	$emails_within_brackets="(";
	foreach($emails_array as $email){
		$emails_within_brackets.="'".$email."',";
	}
	$emails_within_brackets = substr($emails_within_brackets, 0, -1);//enlève dernière virgule
	$emails_within_brackets.=")";
	return $emails_within_brackets;
}

function addNewRestriction($link) {
	//Prétraitement pour récupérer les idUsers en une seule requête.
	$emails_within_brackets = userEmailSplit($_POST["new-restriction"]);
	//Récupération des idUsers
	$idUserS = requestS("SELECT idUser from Users WHERE Email IN $emails_within_brackets");
	if (!array_key_exists('error', $idUserS)) {
		
		// On effectue la suite uniquement si il y a au moins un des mails inscrit dans la liste qui correspond à nos users
		if(count($idUserS) > 0) {
			$_SESSION['error']['restriction-no-user'] = FALSE;
		
			// ajout d'une restriction dans la table restrictions (avec la justification)
			$justification = $_POST['new-restriction']['Justification'];
			$insert1 = insertInDB(array('Justification'=>$justification, 'Restriction_Status'=>'not treated yet' ),'restrictions');
			if (empty($insert1)) {
				
				// récupération de l'idRestriction associé à cette nouvelle restriction
				$select1 = requestS("SELECT MAX(idRestriction) as res FROM restrictions");
				if (!array_key_exists('error', $select1)) {

					$idRes = $select1[0]['res'];

					// association de chacun des users avec une study, puis liaison entre la study et l'idRestriction
					foreach ($_POST["new-restriction"]["idsStudy"] as $key => $idStudy) {
						foreach($idUserS as $idUser){
							$idUser=$idUser['idUser'];
							$insert2 = requestTF("INSERT INTO restrictionlist(idStudy,idUser) VALUES('$idStudy','$idUser')");
							if (!empty($insert2)) {
								$_SESSION['error']['restriction'] = TRUE;
								break 2; // on sort des deux foreach
							}
						}
						$update1 = requestTF("UPDATE studies SET idRestriction = '$idRes' WHERE idStudy = '$idStudy'");
						if (!empty($update1)) {
							$_SESSION['error']['restriction'] = TRUE;
							break 1; 
						}
					}
					if (!isset($_SESSION['error']['restriction']) OR !$_SESSION['error']['restriction']) { // attention
						$_SESSION['error']['restriction'] = FALSE;
						header("Location: $link");
					}
				} else {
					$_SESSION['error']['restriction'] = TRUE;
				}
				
			} else {
				$_SESSION['error']['restriction'] = TRUE;
			}
		} else {
			$_SESSION['error']['restriction-no-user'] = TRUE;
		}
	} else {
		$_SESSION['error']['restriction'] = TRUE;
	}
}

function retryRestriction($link) {
	//Prétraitement pour récupérer les idUsers en une seule requête.
	$emails_within_brackets = userEmailSplit($_POST["edit-restriction"]);
	//Récupération des idUsers
	$idUserS = requestS("SELECT idUser from Users WHERE Email IN $emails_within_brackets");
	if (!array_key_exists('error', $idUserS)) {
		
		// On effectue la suite uniquement si il y a au moins un des mails inscrit dans la liste qui correspond à nos users
		if(count($idUserS) > 0) {
			$_SESSION['error']['restriction-no-user'] = FALSE;

			$idRes =$_POST['edit-restriction']['idRestriction']; // idRestriction? voir studyAndRestrictionPage
			// update de la restriction dans la table restrictions (avec la justification)
			$justification = $_POST['edit-restriction']['Justification'];
			$update1 = requestTF("UPDATE restrictions SET Justification = '$justification' , Restriction_Status = 'not treated yet' WHERE idRestriction = '$idRes' ");
			if (empty($update1)) {
				foreach ($_POST["edit-restriction"]["idsStudy"] as $key => $idStudy) {
					// suppression des anciennes associations user - idStudy pour cette restrciction
					$del1 = requestTF("DELETE FROM restrictionlist WHERE idStudy = '$idStudy'");
					if (empty($del1)) {
						// association de chacun des users avec une study
						foreach($idUserS as $idUser){
							$idUser=$idUser['idUser'];
							$insert1 = requestTF("INSERT INTO restrictionlist(idStudy,idUser) VALUES('$idStudy','$idUser')");
							if (!empty($insert1)) {
								$_SESSION['error']['restriction'] = TRUE;
								break 2; // on sort des deux foreach
							}
						}
					} else {
						$_SESSION['error']['restriction'] = TRUE;
						break;
					}
				}
				if (!isset($_SESSION['error']['restriction']) OR !$_SESSION['error']['restriction']) { // attention
					$_SESSION['error']['restriction'] = FALSE;
					header("Location: $link");
				}
			} else {
				$_SESSION['error']['restriction'] = TRUE;
			}
		} else {
			$_SESSION['error']['restriction-no-user'] = TRUE;
		}
	} else {
		$_SESSION['error']['restriction'] = TRUE;
	}
}


?>